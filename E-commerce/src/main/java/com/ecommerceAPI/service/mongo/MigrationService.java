package com.ecommerceAPI.service.mongo;

import com.ecommerceAPI.model.Product;
import com.ecommerceAPI.repository.ProductRepository;


import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

@Service
public class MigrationService implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private MongoClient mongoClient;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
    }

    public void migrateProductToMongo(Product product) {
        MongoDatabase database = mongoClient.getDatabase("ecommerce_mongo");
        MongoCollection<Document> collection = database.getCollection("products");

        Document doc = new Document("id", product.getId())
                .append("name", product.getName())
                .append("quantity", product.getQuantity())
                .append("price", product.getPrice());
        collection.insertOne(doc);
    }
}

