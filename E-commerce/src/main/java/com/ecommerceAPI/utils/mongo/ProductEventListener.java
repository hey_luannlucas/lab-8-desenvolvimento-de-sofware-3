package com.ecommerceAPI.utils.mongo;

import com.ecommerceAPI.model.Product;
import com.ecommerceAPI.model.mongo.MongoProduct;
import com.ecommerceAPI.repository.mongo.MongoProductRepository;
import jakarta.persistence.PostPersist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

@Component
public class ProductEventListener {

    private final MongoTemplate mongoTemplate;
    private final MongoProductRepository mongoProductRepository;

    @Autowired
    public ProductEventListener(MongoTemplate mongoTemplate, MongoProductRepository mongoProductRepository) {
        this.mongoTemplate = mongoTemplate;
        this.mongoProductRepository = mongoProductRepository;
    }

    @PostPersist
    public void onProductPersist(Product product) {
        // Convertendo o objeto Product do SQL para o formato do MongoDB
        MongoProduct mongoProduct = new MongoProduct();
        mongoProduct.setId(product.getId().toString());
        mongoProduct.setName(product.getName());
        mongoProduct.setQuantity(product.getQuantity());
        mongoProduct.setPrice(product.getPrice());

        // Salvando no MongoDB
        mongoProductRepository.save(mongoProduct);
    }
}


