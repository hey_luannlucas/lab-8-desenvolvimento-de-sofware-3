package com.ecommerceAPI.utils;

import com.ecommerceAPI.model.Product;
import com.ecommerceAPI.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

@Component
public class ProductValidator {

    private final ProductRepository productRepository;

    @Autowired
    public ProductValidator(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public void validateProduct(Product product) {
        Assert.hasText(product.getName(), "Nome do produto não pode ser vazio ou nulo.");
        Assert.isTrue(!productRepository.existsByName(product.getName()), "Nome de produto duplicado: " + product.getName());
        Assert.isTrue(product.getPrice() > 0, "Preço do produto deve ser maior que zero.");
        Assert.isTrue(product.getQuantity() > 0, "Quantidade do produto deve ser maior que zero.");
    }
}
