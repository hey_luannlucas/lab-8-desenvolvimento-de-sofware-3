package com.ecommerceAPI.service;

import com.ecommerceAPI.model.Product;
import com.ecommerceAPI.repository.ProductRepository;
import com.ecommerceAPI.utils.mongo.ProductEventListener;
import com.ecommerceAPI.utils.ProductValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final ProductEventListener productEventListener;
    private final ProductValidator productValidator;

    @Autowired
    public ProductService(ProductRepository productRepository, ProductEventListener productEventListener, ProductValidator productValidator) {
        this.productRepository = productRepository;
        this.productEventListener = productEventListener;
        this.productValidator = productValidator;
    }

    public ResponseEntity<?> saveProduct(Product product) {
        try {
            productValidator.validateProduct(product);

            Product savedProduct = productRepository.save(product);
            productEventListener.onProductPersist(savedProduct);
            return ResponseEntity.ok(savedProduct);
        } catch (IllegalArgumentException e) {
            Map<String, String> response = new HashMap<>();
            response.put("error", e.getMessage());
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response);
        }
    }

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }
}
