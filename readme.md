# API de E-Commerce para Demonstração de Migração de Dados

🛒 Esta é uma API simples de e-commerce desenvolvida para demonstrar o processo de migração de dados de um banco de dados relacional (SQL) para um banco de dados NoSQL (MongoDB).

A API foi construída utilizando Spring Boot, com duas entidades principais: `Product` e `MongoProduct`.

🐳 O Docker está configurado para esta aplicação. Ao iniciar a aplicação, os bancos de dados serão inicializados automaticamente. Portanto, basta ter o Docker rodando.

## Entidades

- **Product**: Esta entidade representa os produtos no banco de dados relacional. Possui os atributos:
    - `id`
    - `name`
    - `quantity`
    - `price`

Esta entidade é persistida utilizando o Hibernate em um banco de dados relacional.

- **MongoProduct**: Esta entidade representa os produtos no banco de dados NoSQL MongoDB. Possui os mesmos atributos que `Product`, mas é persistida em um banco de dados NoSQL MongoDB.

## Migração de Dados

🔄 A migração dos dados ocorre da seguinte forma:

1. Quando um novo produto é criado através da API, o objeto `Product` é persistido no banco de dados relacional utilizando o Hibernate.

2. Um `ProductEventListener` é acionado após a persistência bem-sucedida do objeto `Product`. Este ouvinte converte o objeto `Product` para um objeto `MongoProduct` e o persiste no banco de dados MongoDB.

3. A classe `MigrationService` também é responsável pela migração de dados. Quando a aplicação é iniciada, o método `onApplicationEvent` é chamado. Neste método, todos os produtos presentes no banco de dados relacional são migrados para o banco de dados NoSQL MongoDB.

## Visão Geral das Classes

📚 Visão geral das classes principais da API:

- **ProductController**: Controlador REST responsável por lidar com as requisições relacionadas aos produtos.

- **ProductService**: Camada de serviço que lida com a lógica de negócios relacionada aos produtos.

- **ProductRepository**: Interface responsável pela interação com o banco de dados relacional. Estende `JpaRepository`.

- **MongoProductRepository**: Interface responsável pela interação com o banco de dados MongoDB. Estende `MongoRepository`.

- **ProductValidator**: Classe responsável pela validação dos dados do produto antes de serem persistidos no banco de dados relacional.

- **ProductEventListener**: Ouvinte de eventos que é acionado após a persistência bem-sucedida de um produto no banco de dados relacional. É responsável por converter e persistir os dados do produto no banco de dados MongoDB.

- **MongoConfig**: Configuração do MongoDB.

Com essa estrutura, podemos observar um exemplo simples de migração de dados de um banco de dados relacional para um banco de dados NoSQL, mantendo a consistência dos dados em ambos os bancos.

# Uso

Acesse: `http://localhost:8080/swagger-ui/index.html#/`

1. **Inserindo um produto:**

   ![Inserindo produto](E-commerce/assets/inserir.png)


2. O produto é inserido no PostgreSQL e no MongoDB como pedido no lab:

   ![Sucesso](E-commerce/assets/inserido.png)
