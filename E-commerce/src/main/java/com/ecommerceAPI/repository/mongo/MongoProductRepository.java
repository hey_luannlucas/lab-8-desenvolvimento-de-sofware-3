package com.ecommerceAPI.repository.mongo;

import com.ecommerceAPI.model.mongo.MongoProduct;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MongoProductRepository extends MongoRepository<MongoProduct, String> {
}
